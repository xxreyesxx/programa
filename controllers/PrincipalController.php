<?php

namespace app\controllers;

use Yii;
use app\models\Principal;
use app\models\PrincipalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Detalle;

/**
 * PrincipalController implements the CRUD actions for Principal model.
 */
class PrincipalController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Principal models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PrincipalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Principal model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        $model = $this->findModel($id);
        $searchModel = new \app\models\DetalleSearch();
        $queryParams = array_merge(array(), Yii::$app->request->getQueryParams());
        //$modelEventoCabecera = \app\models\EventoCabecera::find()->select(['id','estado_asistencia'])->where(['estado' => HelperPersonalizado::EVENTO_INICIADO])->all();
        $queryParams["DetalleSearch"]["principal_id"] = $model->id;
        //$queryParamsEvento["EventoDetalleSearch"]["estado_asistencia"] = HelperPersonalizado::ASISTIO_EVENTO_NO;
        $dataProvider = $searchModel->search($queryParams);

        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Principal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Principal();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $cantidad = $model->ingrese_registros;
            if ($model->save()) {
                $contador = 1;
                for ($i = 1; $i <= $cantidad; $i ++) {
                    $detalle = new Detalle();
                    $detalle->principal_id = $model->id;
                    $numero = '135072036-' . $contador;
                    $detalle->clavepredial = $numero;
                    $detalle->cantidad = 1;
                    $detalle->subtotal = 12;
                    $detalle->fecha = $model->fecha;
                    $detalle->save();
                    $contador ++;
                }
                //Creo una conexion con Maria DB que esta en carpeta web con el nombre dbmaria
                $command = Yii::$app->dbmaria->createCommand();
                //Inserto en la tabla principal los mismos campos que obtuve del formulario
                $command->insert('principal', array(
                    'descripcion' => $model->descripcion,
                    'idusuario' => $model->idusuario,
                    'total' => $model->total,
                    'fecha' => $model->fecha,
                ))->execute();

                $id = Yii::$app->dbmaria->getLastInsertId(); //Obtengo el Id de la tabla principal
                $contador2 = 1; // defino un contador 
                //Inserto en la tabla detalle los regisros que deseo
                for ($a = 1; $a <= $cantidad; $a++) {
                    $command->insert('detalle', array(
                        'principal_id' => $id,
                        'clavepredial' => '1325876798-' . $contador2,
                        'cantidad' => 1,
                        'subtotal' => 12,
                        'fecha' => $model->fecha,
                    ))->execute();
                    $contador2 ++;
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Principal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Principal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Principal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Principal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Principal::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
