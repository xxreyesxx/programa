<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "principal".
 *
 * @property int $id
 * @property string $descripcion
 * @property int $idusuario
 * @property float|null $total
 * @property string $fecha
 *
 * @property Detalle[] $detalles
 */
class Principal extends \yii\db\ActiveRecord
{
    
    public $ingrese_registros;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'principal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'idusuario'], 'required'],
            [['idusuario'], 'default', 'value' => null],
            [['idusuario'], 'integer'],
            [['ingrese_registros'], 'integer'],
            [['total'], 'number'],
            [['fecha'], 'safe'],
            [['descripcion'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripción',
            'idusuario' => 'Idusuario',
            'ingrese_registros' => 'Solicitud de Registros',
            'total' => 'Total',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalles()
    {
        return $this->hasMany(Detalle::className(), ['principal_id' => 'id']);
    }
}
