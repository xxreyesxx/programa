<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "detalle".
 *
 * @property int $id
 * @property int $principal_id
 * @property string $clavepredial
 * @property int|null $cantidad
 * @property float|null $subtotal
 * @property string $fecha
 *
 * @property Principal $principal
 */
class Detalle extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'detalle';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['principal_id', 'clavepredial', 'fecha'], 'required'],
            [['principal_id', 'cantidad'], 'default', 'value' => null],
            [['principal_id', 'cantidad'], 'integer'],
            [['subtotal'], 'number'],
            [['fecha'], 'safe'],
            [['clavepredial'], 'string', 'max' => 17],
            [['principal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Principal::className(), 'targetAttribute' => ['principal_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'principal_id' => 'Principal ID',
            'clavepredial' => 'Clavepredial',
            'cantidad' => 'Cantidad',
            'subtotal' => 'Subtotal',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrincipal()
    {
        return $this->hasOne(Principal::className(), ['id' => 'principal_id']);
    }
}
