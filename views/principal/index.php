<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PrincipalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Principal';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="principal-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p style="text-align: center; font-weight: bold; padding-top: 40px">
        <?= Html::a('Registro Principal', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'descripcion',
            'idusuario',
            'total',
            'fecha',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
