<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Principal */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-3"></div>
<div class="col-md-6" style="padding-top: 40px; background-color: #EDFBFF; border-radius: 20px">
<div class="principal-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
    <?= $form->field($model, 'idusuario')->textInput() ?></div>
        <div class="col-md-6">
     <?= $form->field($model, 'fecha')->textInput()
    ?></div>
    
    </div>
    <div class="row">
        <div class="col-md-12">
    <?= $form->field($model, 'ingrese_registros')->textInput() ?></div>
        <div class="col-md-12">
    <?= $form->field($model, 'total')->textInput() ?></div>
        <div class="col-md-12">
     <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?></div>
        
        </div>

    <div class="col-md-6">
        <div class="form-group row" style="padding-top: 25px; margin-left: 87%;" >
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>
<div class="col-md-3"></div>
