<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Principal */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Principals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="principal-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Seguro desea eliminar este item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'descripcion',
            'idusuario',
            'total',
            'fecha',
        ],
    ])
    ?>
</div>
<div class="col-md-8">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
            //'principal_id',
            ['header' => 'Id Principal',
                'value' => function($model) {
                    return $model->principal_id;
                }],
            'clavepredial',
            'cantidad',
            'subtotal',
            'fecha',
                ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
